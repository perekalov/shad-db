#include "fixed.h"

#include "marshal.h"
#include "row.h"
#include "table.h"

#include <cassert>
#include <cstring>

namespace shdb {

class FixedPage : public Page
{
    std::shared_ptr<Frame> frame;
    std::shared_ptr<Marshal> marshal;

    size_t get_row_space()
    {
        return 1 + marshal->get_fixed_row_space();
    }

    uint8_t *get_row_data(RowIndex index)
    {
        return frame->get_data() + index * get_row_space();
    }

    RowIndex get_row_capacity()
    {
        return page_size / get_row_space();
    }

    std::pair<bool, RowIndex> find_row_slot()
    {
        for (RowIndex index = 0; index < get_row_capacity(); ++index) {
            auto *row_data = get_row_data(index);
            if (!static_cast<bool>(row_data[0])) {
                return {true, index};
            }
        }
        return {false, -1};
    }

public:
    FixedPage(std::shared_ptr<Frame> frame, std::shared_ptr<Marshal> marshal)
        : frame(std::move(frame)), marshal(std::move(marshal))
    {}

    virtual RowIndex get_row_count() override
    {
        return get_row_capacity();
    }

    virtual Row get_row(RowIndex index) override
    {
        auto *row_data = get_row_data(index);
        if (static_cast<bool>(row_data[0])) {
            return marshal->deserialize_row(row_data + 1);
        } else {
            return Row();
        }
    }

    virtual void delete_row(RowIndex index) override
    {
        auto *row_data = get_row_data(index);
        *reinterpret_cast<bool *>(row_data) = false;
    }

    virtual std::pair<bool, RowIndex> insert_row(const Row &row) override
    {
        if (auto [found, row_index] = find_row_slot(); found) {
            auto *row_data = get_row_data(row_index);
            *reinterpret_cast<bool *>(row_data) = true;
            marshal->serialize_row(row_data + 1, row);
            return {true, row_index};
        }
        return {false, -1};
    }
};

PageProvider create_fixed_page_provider(std::shared_ptr<Schema> schema)
{
    auto marshal = std::make_shared<Marshal>(std::move(schema));
    return [marshal = std::move(marshal)](std::shared_ptr<Frame> frame) {
        return std::make_shared<FixedPage>(std::move(frame), marshal);
    };
}

}    // namespace shdb
