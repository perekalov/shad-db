#include "bufferpool.h"

#include "malloc.h"

#include <cassert>

namespace shdb {

FramePool::FramePool(std::shared_ptr<Statistics> statistics, FrameIndex frame_count)
    : frames(frame_count)
    , cache([=] {
        std::vector<FrameIndex> free_frames(frame_count);
        FrameIndex index = 0;
        std::generate_n(free_frames.begin(), frame_count, [&] { return index++; });
        return free_frames;
    }())
    , statistics(std::move(statistics))
{
    data = reinterpret_cast<uint8_t *>(pvalloc(frame_count * page_size));
    for (FrameIndex index = 0; index < frame_count; ++index) {
        frames[index].data = data + index * page_size;
    }
}

FramePool::~FramePool()
{
    for (auto &frame : frames) {
        dump_frame(frame);
    }
    free(data);
}

void FramePool::dump_frame(Frame &frame)
{
    if (frame.file) {
        frame.file->write_page(frame.data, frame.page_index);
        ++statistics->page_written;
    }
}

std::pair<FrameIndex, uint8_t *> FramePool::acquire_frame(std::shared_ptr<File> file, PageIndex page_index)
{
    FrameIndex frame_index;
    auto key = std::make_pair(file->get_fd(), page_index);
    if (auto [found, index] = cache.find(key); found) {
        frame_index = index;
        assert(frames[frame_index].page_index == page_index);
        assert(frames[frame_index].file->get_fd() == file->get_fd());
    } else {
        frame_index = cache.put(key);
        auto &frame = frames[frame_index];
        dump_frame(frame);
        frame.file = std::move(file);
        frame.page_index = page_index;
        frame.file->read_page(frame.data, frame.page_index);
        ++statistics->page_read;
    }
    ++frames[frame_index].ref_count;
    if (frames[frame_index].ref_count == 1) {
        cache.lock(key);
    }
    ++statistics->page_accessed;
    return {frame_index, frames[frame_index].data};
}

void FramePool::release_frame(FrameIndex frame_index)
{
    auto &frame = frames[frame_index];
    --frame.ref_count;
    if (frame.ref_count == 0) {
        cache.unlock(std::make_pair(frame.file->get_fd(), frame.page_index));
    }
}

Frame::Frame(std::shared_ptr<FramePool> frame_pool, FrameIndex frame_index, uint8_t *data)
    : frame_pool(std::move(frame_pool)), frame_index(frame_index), data(data)
{}

uint8_t *Frame::get_data()
{
    return data;
}

Frame::~Frame()
{
    frame_pool->release_frame(frame_index);
}

BufferPool::BufferPool(std::shared_ptr<Statistics> statistics, FrameIndex frame_count)
    : frame_pool(std::make_shared<FramePool>(std::move(statistics), frame_count))
{}

std::shared_ptr<Frame> BufferPool::get_page(std::shared_ptr<File> file, PageIndex page_index)
{
    auto [frame_index, data] = frame_pool->acquire_frame(file, page_index);
    return std::make_shared<Frame>(frame_pool, frame_index, data);
}

}    // namespace shdb
