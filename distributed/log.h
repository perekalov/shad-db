#pragma once

#include <iostream>

constexpr int DEBUG_LOG_VERBOSITY = 1;
constexpr int INFO_LOG_VERBOSITY = 2;
constexpr int ERROR_LOG_VERBOSITY = 3;

constexpr int LOG_VERBOSITY = INFO_LOG_VERBOSITY;

class Logger {
public:
  explicit Logger(int level);

  template <class T> Logger &operator<<(const T &t) {
    if (is_enabled()) {
      get_underlying() << t;
    }
    return *this;
  }

  ~Logger() noexcept;

private:
  const int level_;

  bool used_;

  bool is_enabled() const;

  std::ostream &get_underlying();

  std::string format_level() const;
};

#define LOG_DEBUG Logger(DEBUG_LOG_VERBOSITY)
#define LOG_INFO Logger(INFO_LOG_VERBOSITY)
#define LOG_ERROR Logger(ERROR_LOG_VERBOSITY)
