#include "server.h"
#include "log.h"

Server::Server(ActorId id, std::vector<KeyInterval> key_intervals, EnvProxy env)
    : id_(id), key_intervals_(std::move(key_intervals)), env_(env),
      retrier_(id + 88) {}

ActorId Server::get_id() const { return id_; }

void Server::on_tick(Clock &clock, std::vector<Message> messages) {
  std::unordered_map<TransactionId, std::vector<Message>> messages_per_tx;
  // Regardless of input messages every existing transaction must be processed
  // on tick.
  for (const auto &[txid, tx] : transactions_) {
    messages_per_tx[txid].clear();
  }
  for (auto &msg : messages) {
    auto txid = get_message_txid(msg);
    if (txid != UNDEFINED_TRANSACTION_ID) {
      messages_per_tx[txid].push_back(std::move(msg));
    } else {
      report_unexpected_message(msg);
    }
  }
  std::vector<Message> outgoing_messages;
  for (const auto &[txid, tx_messages] : messages_per_tx) {
    auto *tx = get_or_create_transaction(txid);
    tx->tick(clock.next(), tx_messages, &outgoing_messages);
  }
  for (auto &msg : outgoing_messages) {
    env_.send(std::move(msg));
  }
}

TransactionId Server::get_message_txid(const Message &msg) const {
  switch (msg.type) {
  case MSG_START: {
    return msg.get<MessageStartPayload>().txid;
  }
  case MSG_GET: {
    return msg.get<MessageGetPayload>().txid;
  }
  case MSG_PUT: {
    return msg.get<MessagePutPayload>().txid;
  }
  case MSG_COMMIT: {
    return msg.get<MessageCommitPayload>().txid;
  }
  case MSG_COMMIT_ACK: {
    return msg.get<MessageCommitAckPayload>().txid;
  }
  case MSG_ROLLBACK: {
    return msg.get<MessageRollbackPayload>().txid;
  }
  case MSG_ROLLED_BACK_BY_SERVER: {
    return msg.get<MessageRolledBackByServerPayload>().txid;
  }
  case MSG_PREPARE: {
    return msg.get<MessagePreparePayload>().txid;
  }
  case MSG_PREPARE_ACK: {
    return msg.get<MessagePrepareAckPayload>().txid;
  }
  case MSG_CONFLICT: {
    return msg.get<MessageConflictPayload>().txid;
  }
  default: {
    return UNDEFINED_TRANSACTION_ID;
  }
  }
}

void Server::report_unexpected_message(const Message &msg) const {
  LOG_ERROR << "Server " << msg.destination
            << " got un unexpected message of type \""
            << format_message_type(msg.type) << "\" from " << msg.source;
}

ServerTransaction *Server::get_or_create_transaction(TransactionId txid) {
  auto it = transactions_.find(txid);
  if (it == transactions_.end()) {
    it = transactions_
             .emplace(txid, ServerTransaction(id_, &storage_, &retrier_))
             .first;
  }
  return &it->second;
}
