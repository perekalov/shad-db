
extern void TestStorageSimple();
extern void TestClientTransactionSimple();
extern void TestClientServerTransaction();
extern void TestClientServerTwoConflictingTransactions();

int main() {
  TestClientTransactionSimple();
  TestStorageSimple();
  TestClientServerTransaction();
  TestClientServerTwoConflictingTransactions();
  return 0;
}
