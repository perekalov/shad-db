#pragma once

// ClientTransaction orchestrates transaction execution from the client
// perspective. It executes a provided ClientTransactionSpec. The spec defines
// which keys and values should be read, at what timestamp.
//
// Transactions run with snapshot isolation consistency guarantee.
//
// Every transaction is identified by txid, assigned when Start message is sent.
// Within a transaction, all reads are performed at "read_timestamp" time.
// Writes are committed with "write_timestamp" time.
// write_timestamp >= read_timestamp.
//
// ClientTransaction sends and receives messages on behalf of the client.
// When providing messages to ClientTransaction::tick(), the client must filter
// them down by txid, to only include messages relevant to this transaction.
//
// Every transaction may read and write keys that belong to different server
// nodes. To read and write on different nodes, ClientTransaction class uses
// ClientTransactionParticipant class.
//
// All Get operations within a transaction use the same read_timestamp.
// Initially, ClientTransaction obtains read_timestamp from the coordinator
// node.
//
// When the transaction is about to be committed, ClientTransaction picks one
// coordinator node, and sends a Commit message to that node.
// The list of non-coordinator participants is included in that message.
//
// The coordinator performs two-phase-commit to commit the transaction.

#include <cstdint>
#include <memory>
#include <string>
#include <tuple>
#include <unordered_map>
#include <vector>

#include "client_transaction_participant.h"
#include "client_transaction_results.h"
#include "discovery.h"
#include "message.h"
#include "retrier.h"
#include "types.h"

struct ClientTransactionGet {
  Timestamp earliest_timestamp;
  Key key;
};

struct ClientTransactionPut {
  Timestamp earliest_timestamp;
  Key key;
  Value value;
};

// ClientTransactionSpec defines what the transaction needs to do.
struct ClientTransactionSpec {
  // Earliest timestamp to start the transaction.
  Timestamp earliest_start_timestamp;

  // Earliest timestamp to commit the transaction.
  Timestamp earliest_commit_timestamp;

  std::vector<ClientTransactionGet> gets;
  std::vector<ClientTransactionPut> puts;

  enum { UNDEFINED, COMMIT, ROLLBACK } action;
};

enum class ClientTransactionState {
  // Initial state, before Start message is sent.
  NOT_STARTED,

  // Start message has been sent, and txid assigned.
  START_SENT,

  // Start message has been sent and acknowledged by the coordinator.
  OPEN,

  // Rollback has been requested.
  ROLLBACK_SENT,

  // Rollback confirmed by server.
  ROLLED_BACK,

  // Rolled back by server, due to conflict with another transaction.
  ROLLED_BACK_BY_SERVER,

  // Commit requested
  COMMIT_SENT,

  // Commited by server.
  COMMITTED,
};

std::string format_client_transaction_state(ClientTransactionState state);

class ClientTransaction {
public:
  ClientTransaction(ActorId self, ClientTransactionSpec spec,
                    const Discovery *discovery, IRetrier *rt);

  void tick(Timestamp ts, const std::vector<Message> &messages,
            std::vector<Message> *msg_out);

  ClientTransactionState state() const { return state_; }
  ClientTransactionResults export_results() const;

  ActorId id() const { return self_; }
  TransactionId get_id() const;

private:
  enum class ParticipantState { NOT_STARTED, START_SENT, STARTED };

  enum class RequestState { NOT_STARTED, STARTED, COMPLETED };

  struct GetState {
    RequestState request_state{RequestState::NOT_STARTED};
    IRetrier::Handle rt_handle{IRetrier::UNDEFINED_HANDLE};
    Value value;
  };

  struct PutState {
    RequestState request_state{RequestState::NOT_STARTED};
    IRetrier::Handle rt_handle{IRetrier::UNDEFINED_HANDLE};
  };

  // Process replies, in corresponding states.
  void process_replies_not_started(const std::vector<Message> &messages);
  void process_replies_start_sent(const std::vector<Message> &messages);
  void process_replies_open(const std::vector<Message> &messages);
  void process_replies_commit_sent(const std::vector<Message> &messages);
  void process_replies_rollback_sent(const std::vector<Message> &messages);
  void process_replies_commited(const std::vector<Message> &messages);
  void process_replies_rolled_back(const std::vector<Message> &messages);
  void
  process_replies_rolled_back_by_server(const std::vector<Message> &messages);

  void handle_rolled_back_by_server(const Message &);

  std::tuple<int, int> completed_requests() const;

  void maybe_init_participant(ActorId id, Timestamp ts);
  void report_unexpected_msg(const Message &);

  std::unordered_map<ActorId, std::unique_ptr<ClientTransactionParticipant>> p_;

  std::vector<ActorId> get_participants_except_coordinator() const;

  const ActorId self_;
  const ClientTransactionSpec spec_;
  const Discovery *const discovery_;
  IRetrier *const rt_;

  TransactionId txid_{UNDEFINED_TRANSACTION_ID};
  ActorId coordinator_{UNDEFINED_ACTOR_ID};
  ClientTransactionState state_{ClientTransactionState::NOT_STARTED};
  TransactionId conflict_txid_{UNDEFINED_TRANSACTION_ID};
  IRetrier::Handle commit_handle_{IRetrier::UNDEFINED_HANDLE};

  std::unordered_map<ActorId, IRetrier::Handle> rollback_handle_;

  Timestamp read_timestamp_{UNDEFINED_TIMESTAMP};
  Timestamp commit_timestamp_{UNDEFINED_TIMESTAMP};

  size_t next_get_{0};
  size_t next_put_{0};
};
