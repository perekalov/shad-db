#pragma once

#include <vector>

#include "types.h"

// [key_begin, key_end)
struct KeyInterval {
  Key begin;
  Key end;
};

// Discovery finds node id for a key.
class Discovery {
public:
  // Single-node cluster.
  explicit Discovery(ActorId id);

  // Multiple-node cluster.
  Discovery(std::vector<ActorId> ids, std::vector<KeyInterval> key_intervals);

  ActorId get_by_key(Key key) const;
  ActorId get_random() const;

private:
  const std::vector<ActorId> ids_;
  const std::vector<KeyInterval> key_intervals_;
};
