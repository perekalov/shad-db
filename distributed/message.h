#pragma once

// Message is a typed unit of communication.
// There are few defined message types, see enum MessageType below.
// Each type comes with type-specific information, defined in message_specific.h

// Every message has a unique identifier that does not change with retried.
// Reply messages refer to the request id.
// Messages within the transaction refer to txid, which is the id of
// the start message.

#include <string>
#include <variant>
#include <vector>

#include "message_specific.h"
#include "types.h"

enum MessageType {
  MSG_UNDEFINED,

  // client -> server: request start of transaction.
  MSG_START,

  // server -> client: acknowledge start of transaction.
  MSG_START_ACK,

  // client -> server: get(key)
  MSG_GET,

  // server -> client: respond with value for a key.
  MSG_GET_REPLY,

  // client -> server: put(key, value)
  MSG_PUT,

  // server -> client: acknowledge writing a key.
  MSG_PUT_REPLY,

  // server -> client: inform client that server rolled back transaction due to
  // conflict. coordinator server -> participant server: inform other
  // participants that
  //  transaction is aborted due to conflict.
  MSG_ROLLED_BACK_BY_SERVER,

  // coordinator server -> participant server: prepare for commit
  MSG_PREPARE,

  // participant server -> coordinator server: acknowledge prepare for commit
  MSG_PREPARE_ACK,

  // participant server -> coordinator server: unable to prepare because of
  // conflict.
  MSG_CONFLICT,

  // client -> coordinator server: ask to commit transaction
  MSG_COMMIT,

  // coordinator server -> client: transaction is committed.
  // coordinator server -> participant server: transaction is committed.
  MSG_COMMIT_ACK,

  // client -> server: ask to rollback transaction.
  MSG_ROLLBACK,

  // server -> client: acknowledge transaction rollback.
  MSG_ROLLBACK_ACK
};

std::string format_message_type(MessageType type);

struct Message {
  MessageType type{MSG_UNDEFINED};
  ActorId source{-1};
  ActorId destination{-1};

  // Globally unique message identifier. Can be used to differentiate requests
  // when processing the response.
  //
  // Does not change when request is retried.
  int64_t id;

  std::variant<MessageStartPayload, MessageStartAckPayload, MessageGetPayload,
               MessageGetReplyPayload, MessagePutPayload,
               MessagePutReplyPayload, MessageRolledBackByServerPayload,
               MessagePreparePayload, MessagePrepareAckPayload,
               MessageConflictPayload, MessageCommitPayload,
               MessageCommitAckPayload, MessageRollbackPayload,
               MessageRollbackAckPayload>
      payload;

  template <class T> const T &get() const { return std::get<T>(this->payload); }

  template <class T> T &get() { return std::get<T>(this->payload); }
};

// Factory functions for message objects.
Message CreateMessageStart(ActorId source, ActorId destination,
                           TransactionId txid = UNDEFINED_TRANSACTION_ID,
                           Timestamp start_timestamp = UNDEFINED_TIMESTAMP);

Message CreateMessageStartAck(ActorId source, ActorId destination,
                              TransactionId txid, Timestamp start_timestamp);
Message CreateMessageGet(ActorId source, ActorId destination,
                         TransactionId txid, Key key);
Message CreateMessagePut(ActorId source, ActorId destination,
                         TransactionId txid, Key key, Value value);
Message CreateMessageGetReply(ActorId source, ActorId destination,
                              TransactionId txid, int req_id, Value value);
Message CreateMessagePutReply(ActorId source, ActorId destination,
                              TransactionId txid, int req_id);
Message CreateMessageRolledBackByServer(ActorId source, ActorId destination,
                                        TransactionId txid,
                                        int64_t conflict_txid);
Message CreateMessageCommit(ActorId source, ActorId destination,
                            TransactionId txid,
                            std::vector<ActorId> participants);
Message CreateMessageCommitAck(ActorId source, ActorId destination,
                               TransactionId txid, Timestamp commit_timestamp);
Message CreateMessageRollback(ActorId source, ActorId destination,
                              TransactionId txid);
Message CreateMessageRollbackAck(ActorId source, ActorId destination,
                                 TransactionId txid);

// Messages Prepare, PrepareAck, ConflictAck are only used in server-to-server
// communication within the two-phase-commit protocol.
Message CreateMessagePrepare(ActorId source, ActorId destination,
                             TransactionId txid, Timestamp commit_timestamp);
Message CreateMessagePrepareAck(ActorId source, ActorId destination,
                                TransactionId txid);
Message CreateMessageConflict(ActorId source, ActorId destination,
                              TransactionId txid, TransactionId conflict_txid);
