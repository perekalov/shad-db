#pragma once

#include "types.h"

class Clock {
public:
  Timestamp current() const;
  Timestamp next();

private:
  Timestamp next_{0};
};
