
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>

void load_state(const char* filename) {
  // Load state from file "filename".
}

int open_tcp(int port) {
  // Open a TCP socket, bind to the given port, and return the socket number
  return 0;
}

int main(int argc, char* argv[]) {
  if (argc < 3) {
    fprintf(stderr, "Usage: main <port number> <state filename>\n");
    exit(1);
  }

  int port = atoi(argv[1]);
  const char* filename = argv[2];

  load_state(filename);
  open_tcp(port);

  // Accept incoming clients, and serve requests.

  return 0;
}
