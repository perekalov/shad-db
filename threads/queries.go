package main

import (
  "fmt"
  "log"
  "os"
  "sync"
  "time"
)

type qmSlot struct {
  mu sync.Mutex
  qm map[int]*queryManager
}

type queriesManager struct {
  slot []qmSlot
}

type interval struct {
  begin int64
  end int64
}

type readQuery struct {
  i interval
  qm* queryManager
  value* string
}

type writeQuery struct {
  i interval
  qm* queryManager
  value string
}

type queryManager struct {
  mu sync.Mutex

  // "reads" and "writes" are ordered by "begin"
  reads []*readQuery
  writes []*writeQuery
}

const kSlots = 16386

func createQueriesManager() *queriesManager {
  return &queriesManager{
    slot: make([]qmSlot, kSlots),
  }
}

func (self *queriesManager) get(key int) *queryManager {
  index := key % kSlots
  slot := &self.slot[index]
  slot.mu.Lock()
  defer slot.mu.Unlock()

  if slot.qm == nil {
    slot.qm = make(map[int]*queryManager)
  }

  val, ok := slot.qm[key]

  if !ok {
    val = createQueryManager()
    slot.qm[key] = val
  }

  return val
}

const kMaxInt64 = (1 << 63) - 1

func dump(r *readQuery, writes []*writeQuery) {
  s := int64(kMaxInt64)
  for _, w := range writes {
    if w.i.begin > 1 && w.i.begin < s {
      s = w.i.begin
    }
  }
  fmt.Fprintf(os.Stderr, "r '%s' [%d, %d)\n\n", *r.value, r.i.begin - s, r.i.end - s)

  for _, w := range writes {
    fmt.Fprintf(os.Stderr, "w '%s' [%d, %d)\n", w.value, w.i.begin - s, w.i.end - s)
  }
}

// Should not be called concurrently with other calls
func (self* queriesManager) validate() error {
  for _, s := range self.slot {
    for _, qm := range s.qm {
      v := &validator{}
      v.init(qm.writes)
      for _, r := range qm.reads {
        err := v.validate(r)
        if err != nil {
          dump(r, qm.writes)
          return err
        }
      }
    }
  }

  return nil
}

func createQueryManager() *queryManager {
  ret := &queryManager{}
  ret.writes = make([]*writeQuery, 1)
  ret.writes[0] = &writeQuery{
    i: interval{
      begin: 1,
      end: 2,
    },
    value: "undefined",
  }
  return ret
}

func fetchNow() int64 {
  return time.Now().UnixNano() / 100
}

func (self* queryManager) startRead() *readQuery {
  self.mu.Lock()
  defer self.mu.Unlock()

  q := &readQuery{qm: self}
  q.i.begin= fetchNow()

  self.reads = append(self.reads, q)

  return q
}

func (self* queryManager) startWrite(value string) *writeQuery {
  self.mu.Lock()
  defer self.mu.Unlock()

  q := &writeQuery{qm: self}
  q.i.begin = fetchNow()
  q.value = value

  self.writes = append(self.writes, q)

  return q
}

func (self* writeQuery) completed(response string) {
  if response != "OK" {
    log.Fatalf("Invalid write response: '%s'\n", response)
  }

  self.qm.mu.Lock()
  defer self.qm.mu.Unlock()
  self.i.end = fetchNow()
}

func (self* readQuery) completed(response string) {
  self.qm.mu.Lock()
  defer self.qm.mu.Unlock()
  self.value = &response
  self.i.end = fetchNow()
}
