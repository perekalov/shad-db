#!/bin/bash

set -e
set -x

usernames=(
  "kvk1920"
  "sgjurano"
  "semin-serg"
  "pew-pew"
  "mkspopov"
  "SYury"
  "yuryalekseev"
  "cezarnik"
  "quid"
  "s-reznick"
  "vart"
  "dnorlov"
)

git clone git@gitlab.com:savrus/shad-db.git

for username in ${usernames[@]}
do
  url="git@gitlab.manytask.org:db-spring-2021/${username}.git"
  git clone ${url}
  cd ${username}
  git checkout submits/threads
  cd ..
  echo "Done checking out ${username}"
done
