#!/usr/local/bin/python3

import sys

blacklist = (
  ("mkspopov", 4),
  ("sgjurano", 4),
  ("sgjurano", 5),
  ("yuryalekseev", 1),
  ("yuryalekseev", 2),
  ("yuryalekseev", 3),
  ("yuryalekseev", 4),
)

if (sys.argv[1], int(sys.argv[2])) not in blacklist:
  sys.exit(1)
