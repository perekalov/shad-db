#!/bin/bash


user_commit=(
  "kvk1920" "329733de0f2fb58b2f9ae0fbdbacd7a29179d298"
  "sgjurano" "f6dfa633fc6fa454d219dd5b84732c205eb20171"
  "semin-serg" "b325e33e1a1f93fb1b95a9de206da0f23457b705"
  "pew-pew" "ede9fd0593e0d1b7a3f58527cc8ffb984c835a1a"
  "mkspopov" "ba335224ea54a8c47a7dc978b157185ca83e617c"
  "SYury" "1b5e75c4e4089590651907f6a2993b9c366e0ae7"
  "yuryalekseev" "df13455d30a2e4945643f82bafe1f1fe8b42500f"
  "cezarnik" "40879ac09e4f4cf3c58146fe45f7b1609f374145"
  "quid" "036aecc945b25275b2f3f6f955a064222f18d017"
  # s-reznick had another passing commit, 5 minutes after the deadline
  "s-reznick" "7c6828715a9582d7877ae04f37d9755726c7cf93"
  "vart" "87611b16a111177b60641a4b2055fd6ddeb94cbc"
  # Reference implementation
  "dnorlov" "fe7e3813facd793a48f1320dd4cccb35a2feb320"
)

len=${#user_commit[@]}
let limit=len/2-1

for i in `seq 0 $limit`
do
  let u=2*i
  let v=2*i+1
  username=${user_commit[$u]}
  commit=${user_commit[$v]}
  cd $username
  git checkout $commit
  cd ..
done
