#pragma once

#include "table.h"

namespace shdb {

class ScanIterator
{
    // Your code goes here.
public:
    ScanIterator(std::shared_ptr<Table> table, PageIndex page_index, RowIndex row_index);

    RowId get_row_id() const;
    Row get_row();
    Row operator*();
    bool operator==(const ScanIterator &other) const;
    bool operator!=(const ScanIterator &other) const;
    ScanIterator &operator++();
};

class Scan
{
    // Your code goes here.
public:
    explicit Scan(std::shared_ptr<Table> table);
    ScanIterator begin() const;
    ScanIterator end() const;
};

}    // namespace shdb
